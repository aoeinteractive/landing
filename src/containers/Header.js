import styled from 'styled-components';
import Image from '../assets/images/bg.jpg';

const Header = styled.div`
  background-position:50% 0%;
  background: url(${Image}) no-repeat center top fixed;
  background-size: cover;
  text-align:center;
`;

export default Header;