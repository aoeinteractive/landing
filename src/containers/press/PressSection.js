import React, { Component } from 'react';
import styled from 'styled-components';

import RedditLogo from '../../assets/images/reddit-logo2.png';
import ProductHuntLogo from '../../assets/images/producthunt-logo.png';
import HeavyLogo from '../../assets/images/heavy-logo.png';

import PressItem from '../../components/PressItem';

const PressList = styled.ul`
  border-bottom:1px solid #dadfe3;
  margin-bottom:0;
  padding-left:0;
  margin-top:0;
`;

class PressSection extends Component {
  render() {
    return (
      <PressList>
        <PressItem link="https://www.reddit.com/r/pokemongo/comments/50j111/after_many_long_nights_of_coding_im_happy_to/" 
          image={RedditLogo} />
        <PressItem link="https://www.producthunt.com/posts/catch-em-go" 
          image={ProductHuntLogo} />
        <PressItem link="http://heavy.com/games/2016/08/pokemon-go-how-to-find-other-players-in-the-your-my-area-social-network-service-map-track-trainers-meetups-local-catch-em-go/" 
          image={HeavyLogo} />
      </PressList>
    );
  }
}

export default PressSection;
