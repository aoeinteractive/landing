import styled from 'styled-components';

const Overlay = styled.div`
  padding-top:84px;
  padding-bottom:175px;
  height:auto;
`;

export default Overlay;