import React, { Component } from 'react';
import styled from 'styled-components';
import {Row} from 'react-bootstrap';
import NewsletterSignup from '../../components/NewsletterSignup';

const IntroContainer = styled.div`
  position:relative;
  min-height:1px;
  padding-left:15px;
  padding-right:15px;
  @media (min-width:992px) {
    float:left;
    width:100%;
  }
  &:after {
    clear:both;
  }
`;

const IntroBox = styled.div`
  margin-top:80px;
`;

const IntroHeader = styled.h1`
  color:#fff;
  font-size:55px;
  line-height:5rem;
  font-weight:400;
`;

const IntroText = styled.div`
  color:#fff;
  font-size:18px;
  line-height:28px
  font-weight:500;
  margin-top:15px;
  margin-bottom:45px;
`;

const FormContainer = styled.div`
  position: relative;
  min-height: 1px;
  padding-left: 15px;
  padding-right: 15px;
  @media (min-width: 768px) {
    margin-left:8.33333333%
    width:83.33333333%;
    float:left;
  }
`;

const FormBg = styled.div`
  display:inline-block;
  background:rgba(0, 0, 0, 0.45);
  border-radius:6px;
  padding:25px 25px 25px 25px;
`;

class IntroSection extends Component {
  render() {
    return (
      <IntroContainer>
        <IntroBox>
          <IntroHeader>Find, Connect, Share.</IntroHeader>
          <IntroText>
            <p>Connect with other Pokemon Go trainers and find out what's happening near you.</p>
            <p>Subscribe to be the first to know when we relaunch.</p>
          </IntroText>
          <Row>
            <FormContainer> 
              <FormBg>
                <NewsletterSignup />
              </FormBg>
            </FormContainer>
          </Row>
        </IntroBox>
      </IntroContainer>
    );
  }
}

export default IntroSection;