import React, { Component } from 'react';
import styled from 'styled-components';

const MessageWrapper = styled.p`
  color:#fff;
  margin-top:15px;
`;

const MessageIcon = styled.span`
  color: ${(props) => props.success ? "green" : "red"}
  margin-right:10px;
`;

class MailchimpMessage extends Component {

  renderIcon() {
    if (this.props.success) {
      return <MessageIcon success className={"fa fa-" + this.props.icon}/>
    } else {
      return <MessageIcon error className={"fa fa-" + this.props.icon}/>
    }
  }

  render() {
    return (
      <MessageWrapper>
        {this.renderIcon()}
        {this.props.message}
      </MessageWrapper>
    );
  }
}

export default MailchimpMessage;