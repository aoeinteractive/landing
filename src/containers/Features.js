import styled from 'styled-components';

const Features = styled.section`
  border-top:1px solid #eaebec;
  border-bottom:1px solid #eaebec;
  background:#F7F8FA;
`;

export default Features;