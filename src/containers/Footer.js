import React, { Component } from 'react';
import styled from 'styled-components';
import FooterSocialItem from '../components/FooterSocialItem';

const FooterWrapper = styled.footer`
  padding-top:40px;
  padding-bottom:40px;
  border-top:1px solid #eaebec;
  border-bottom:1px solid #eaebec;
  background:#F7F8FA;
`;

const FooterSocialWrapper = styled.ul`
  margin-top:10px;
`;

class Footer extends Component {
  render() {
    return (
      <div>
        <FooterWrapper>
          ©2017 AoE Interactive LLC. All Rights Reserved
          <FooterSocialWrapper>
            <FooterSocialItem icon="facebook" link="https://facebook.com/CatchEmGo" />
            <FooterSocialItem icon="twitter" link="https://twitter.com/CatchEmGo" />
            <FooterSocialItem icon="envelope" link="mailto:info@catchemgo.com" />
          </FooterSocialWrapper>
        </FooterWrapper>
      </div>
    );
  }
}


export default Footer;