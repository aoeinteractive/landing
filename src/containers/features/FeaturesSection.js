import React, { Component } from 'react';
import styled from 'styled-components';
import {Row, Col} from 'react-bootstrap';
import FeatureItem from '../../components/FeatureItem';

import Globe from '../../assets/images/globe.png';
import People from '../../assets/images/people.png';
import Medal from '../../assets/images/medal.png';

const FeaturesHeader = styled.div`
  margin-top:90px;
  margin-bottom:60px;
`;

const FeaturesTitle = styled.div`
  color:#cb9b27;
  text-transform: uppercase;
  font-size:14px;
  font-weight:600;
`;

const FeaturesH2 = styled.h2`
  margin-bottom:20px;
  margin-top:10px;
  color:#454545;
  font-size:32px;
  line-height:3.125rem;
  font-weight:400;
`;

const FeaturesLine = styled.div`
  background:#cb9b27;
  margin:auto;
  height:2px;
  width:80px;
  display:block;
`;

const FeaturesSubtext = styled.div`
  margin-top:20px;
`;

class FeaturesSection extends Component {
  render() {
    return (
      <div>
        <FeaturesHeader>
          <FeaturesTitle>Features</FeaturesTitle>
          <FeaturesH2>Pokemon Go Near You</FeaturesH2>
          <FeaturesLine></FeaturesLine>
          <FeaturesSubtext>Discover local events and meetups, or create your own and meet other trainers near you.</FeaturesSubtext>
        </FeaturesHeader>
        <Row>
          <Col md={4}>
            <FeatureItem image={Globe} title="Find" description="Find out what's happening near you. Discover all the awesome Pokemon Go meetups and events in your area."/>
          </Col>
          <Col md={4}>
            <FeatureItem image={People} title="Connect" description="Connect with other Pokemon Go fans near you and around the world, and plan your next adventure."/>
          </Col>
          <Col md={4}>
            <FeatureItem image={Medal} title="Share" description="Create your own meetups and share the experience with other Pokemon Go fans in your area."/>
          </Col>
        </Row>
      </div>
    );
  }
}

export default FeaturesSection;
