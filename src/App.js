import React, { Component } from 'react';
import Header from './containers/Header';
import Overlay from './containers/header/Overlay';
import {Row} from 'react-bootstrap';
import ReactGA from 'react-ga';

import LogoImage from './assets/images/logo.png';
import IntroSection from './containers/header/IntroSection';
import Press from './containers/Press';
import PressSection from './containers/press/PressSection';
import Features from './containers/Features';
import FeaturesSection from './containers/features/FeaturesSection';

import Footer from './containers/Footer';

class App extends Component {
  constructor() {
    super();
    ReactGA.initialize('UA-47994362-5');
    ReactGA.pageview(window.location.pathname);
  }
  render() {
    return (
      <div>
        <Header>
          <Overlay>
            <div className="container">
              <div>
                <img src={LogoImage} alt="Catch Em Go Logo" />
              </div>
              <Row>
                <IntroSection />
              </Row>
            </div>
          </Overlay>
        </Header>
        <Press>
          <PressSection />
        </Press>
        <Features>
          <div className="container">
            <FeaturesSection />
          </div>
        </Features>
        <Footer />
      </div>
    );
  }
}

export default App;
