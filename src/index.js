import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './global-styles.js';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
