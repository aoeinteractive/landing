import { injectGlobal } from 'styled-components';

injectGlobal`
  body {
    margin: auto;
    font-size:16px;
    line-height:28px;
    font-weight:300;
    color:#313131;
    text-align:center;
    overflow-x:hidden
    font-family: 'Roboto';
  }
`;