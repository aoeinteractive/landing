import React, { Component } from 'react';
import isEmail from 'validator/lib/isEmail';
import MailchimpMessage from '../containers/header/MailchimpMessage';
import styled from 'styled-components';

const Form = styled.form`
  padding-right:6px;
`;

const EmailInput = styled.input`
  min-height:65px;
  width:350px;
  font-size:inherit;
  margin:3px;
  outline:0;
  box-shadow:none;
  border:1px solid #dedede;
  border-radius:3px;
  color:#999;
  padding-left:21px;
  height:34px;
  line-height:1.42857143
  background-color:#fff
  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  font-family:inherit;
  @media (min-width:768px) {
    display:inline-block;
    vertical-align:middle;
  }
`;

const SubmitButton = styled.button`
  margin:3px;
  box-shadow:none;
  border-radius:3px;
  text-transform:uppercase;
  letter-spacing:1px;
  font-size:16px;
  line-height:1.6;
  border:none;
  padding:18px 40px 18px 40px;
  color:#fff;
  transition:none;
  font-weight:600;
  display:inline-block;
  text-align:center;
  touch-action:manipuation;
  vertical-align:middle;
  cursor:pointer;
  white-space:nowrap;
  user-select:none;
  background:#f0c802;
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0%, #f0c802), color-stop(100%, #cb9b27));
  background:linear-gradient(to bottom, #f0c802 0%, #cb9b27 100%);
  &:hover {
    background:#cb9b27;
  }
`;

const HiddenField = styled.div`
  position: absolute; 
  left: -5000px;
`;

class NewsletterSignup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
      error: false,
      canSubmit: false,
      email: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({email: e.target.value});
    if (e.target.value.length) {
      this.setState({canSubmit: true});
    } else {
      this.setState({
        canSubmit: false, 
        error: false, 
        success: false
      });
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    if (isEmail(this.state.email)) {
      e.target.submit();
      this.setState({ 
        email: '',
        success: true, 
        error: false, 
        canSubmit: false
      });
    } else {
      this.setState({
        error: true, 
        success: false
      });
    }
  }

  renderMessage() {
    if (this.state.success) {
      return <MailchimpMessage success icon="check" message="Thank you for subscribing! Please check your email to confirm." />;
    }
    if (this.state.error) {
      return <MailchimpMessage success={false} icon="exclamation-triangle" message="Please enter a valid email." />;
    }
  }

  render() {
    return (
      <div>
        <Form onSubmit={this.handleSubmit}
              action="//catchemgo.us9.list-manage.com/subscribe/post?u=31ca6c390493e9f3bb6a6ee66&amp;id=e00ac69ee9"
              method="post"
              target="_blank"
              id="mc-embedded-subscribe-form"
              name="mc-embedded-subscribe-form">
          <EmailInput type="email" 
                      name="EMAIL" 
                      id="mce-EMAIL" 
                      className="email"
                      placeholder="Enter your Email" 
                      value={this.state.email} 
                      onChange={this.handleChange} />
          <HiddenField aria-hidden="true">
            <input type="text" name="b_31ca6c390493e9f3bb6a6ee66_e00ac69ee9" tabIndex="-1" value="" />
          </HiddenField>
          <SubmitButton disabled={!this.state.canSubmit}>Subscribe</SubmitButton>
        </Form>
        {this.renderMessage() }
      </div>
    );
  }
}

export default NewsletterSignup;