import React, { Component } from 'react';
import styled from 'styled-components';

const SingleFeature = styled.div`
  padding: 40px 35px 40px 35px;
  margin: 0 0 100px 0;
  background: #ffffff;
  border-top: 1px solid #eaeaea;
  border-left: 1px solid #eaeaea;
  border-right: 1px solid #eaeaea;
  border-bottom: 4px solid #eaeaea;
  border-radius: 3px;
  -webkit-transition: all ease 0.55s;
  transition: all ease 0.55s;
  &:hover {
    border-bottom-color:#cb9b27;
  }
`;

const FeatureIcon = styled.div`
  margin-bottom:35px;
`;

const FeatureTitle = styled.h3`
  margin-bottom:10px
  font-size: 24px;
  line-height: 2.375rem;  
  margin-top: 20px;
  font-family: inherit;
  font-weight: 500;
`;

const FeatureDescription = styled.p`
  margin: 0 0 10px;
`;


class FeatureItem extends Component {
  render() {
    return (
      <div>
        <SingleFeature>
          <FeatureIcon>
            <img src={this.props.image} alt={this.props.title} />
          </FeatureIcon>
          <FeatureTitle>{this.props.title}</FeatureTitle>
          <FeatureDescription>{this.props.description}</FeatureDescription>
        </SingleFeature>
      </div>
    );
  }
}

export default FeatureItem;