import React, { Component } from 'react';
import styled from 'styled-components';

const PressList = styled.li`
  display:inline-block;
  margin:25px 25px 25px 25px;
`;

const PressLogo = styled.img`
  max-height:40px;
  opacity:0.6;
  &:hover {
    opacity:1;
    cursor:pointer;
  }
`;

class PressItem extends Component {
  render() {
    return (
      <PressList>
        <a href={this.props.link} target="_blank">
          <PressLogo src={this.props.image} />
        </a>
      </PressList>
    );
  }
}

export default PressItem;