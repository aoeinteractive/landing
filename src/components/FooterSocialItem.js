import React, { Component } from 'react';
import styled from 'styled-components';
import FontAwesome from 'react-fontawesome';

const FooterSocial = styled.li`
  display:inline-block;
  margin-right:5px;
  margin-left:5px;
`;

const FooterSocialA = styled.a`
  text-decoration:none;
  color:#313131;
  opacity:0.5;
  &:hover {
    opacity:1;
    color:#313131;
  }
`;

class FooterSocialItem extends Component {
  render() {
    return (
      <FooterSocial>
        <FooterSocialA href={this.props.link} target="_blank">
          <FontAwesome name={this.props.icon} />
        </FooterSocialA>
      </FooterSocial>
    );
  }
}

export default FooterSocialItem;